class ChatsController < ApplicationController
  before_action :authenticate_usuario!
  def show
    @messages = Message.all
  end
end
